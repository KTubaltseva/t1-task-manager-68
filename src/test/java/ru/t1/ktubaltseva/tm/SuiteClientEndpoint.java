package ru.t1.ktubaltseva.tm;

import org.junit.runner.RunWith;
        import org.junit.runners.Suite;
import ru.t1.ktubaltseva.tm.endpoint.ProjectEndpointTest;
import ru.t1.ktubaltseva.tm.endpoint.TaskEndpointTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({ProjectEndpointTest.class, TaskEndpointTest.class} )
public class SuiteClientEndpoint {

}