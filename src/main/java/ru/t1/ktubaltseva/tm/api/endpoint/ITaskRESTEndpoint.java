package ru.t1.ktubaltseva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import java.util.List;

public interface ITaskRESTEndpoint {

    @NotNull
    TaskDTO create() throws AbstractException;

    @NotNull
    TaskDTO add(@NotNull TaskDTO model) throws AbstractException;

    void deleteById(@NotNull String id) throws AbstractException;

    boolean existsById(@NotNull String id) throws AbstractException;

    @NotNull
    TaskDTO findById(@NotNull String id) throws AbstractException;

    @NotNull
    TaskDTO update(@NotNull TaskDTO model) throws AbstractException;

    @NotNull
    List<TaskDTO> findAll();

    void clear();

    long count();

}
