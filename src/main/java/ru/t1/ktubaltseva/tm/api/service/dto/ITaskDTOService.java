package ru.t1.ktubaltseva.tm.api.service.dto;

import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;

public interface ITaskDTOService extends IDTOService<TaskDTO> {

}
