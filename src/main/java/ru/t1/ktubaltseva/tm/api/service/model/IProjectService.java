package ru.t1.ktubaltseva.tm.api.service.model;

import ru.t1.ktubaltseva.tm.model.Project;

public interface IProjectService extends IService<Project> {

}
