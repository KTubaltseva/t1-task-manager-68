package ru.t1.ktubaltseva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import java.util.List;

public interface IProjectRESTEndpoint {

    @NotNull
    ProjectDTO create() throws AbstractException;

    @NotNull
    ProjectDTO add(@NotNull ProjectDTO model) throws AbstractException;

    void deleteById(@NotNull String id) throws AbstractException;

    boolean existsById(@NotNull String id) throws AbstractException;

    @NotNull
    ProjectDTO findById(@NotNull String id) throws AbstractException;

    @NotNull
    ProjectDTO update(@NotNull ProjectDTO model) throws AbstractException;

    @NotNull
    List<ProjectDTO> findAll();

    void clear();

    long count();
}
