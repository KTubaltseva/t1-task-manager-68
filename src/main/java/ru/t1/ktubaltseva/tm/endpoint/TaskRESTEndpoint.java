package ru.t1.ktubaltseva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktubaltseva.tm.api.endpoint.ITaskRESTEndpoint;
import ru.t1.ktubaltseva.tm.api.service.dto.ITaskDTOService;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public final class TaskRESTEndpoint implements ITaskRESTEndpoint {

    @Autowired
    private ITaskDTOService service;

    @NotNull
    @Override
    @PutMapping("/create")
    public TaskDTO create() throws AbstractException {
        return service.create();
    }

    @NotNull
    @Override
    @PutMapping("/add")
    public TaskDTO add(@RequestBody @NotNull final TaskDTO task) throws AbstractException {
        return service.add(task);
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void deleteById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        service.deleteById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        return service.existsById(id);
    }

    @NotNull
    @Override
    @GetMapping("/findById/{id}")
    public TaskDTO findById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        return service.findById(id);
    }

    @NotNull
    @Override
    @PostMapping("/update/{id}")
    public TaskDTO update(@RequestBody @NotNull final TaskDTO task) throws AbstractException {
        return service.update(task);
    }

    @NotNull
    @Override
    @GetMapping("/findAll")
    public List<TaskDTO> findAll() {
        return service.findAll();
    }


    @Override
    @DeleteMapping("/clear")
    public void clear() {
        service.clear();
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return service.count();
    }

}

