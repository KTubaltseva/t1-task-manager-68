package ru.t1.ktubaltseva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktubaltseva.tm.api.endpoint.IProjectRESTEndpoint;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectDTOService;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public final class ProjectRESTEndpoint implements IProjectRESTEndpoint {

    @Autowired
    private IProjectDTOService service;

    @NotNull
    @Override
    @PutMapping("/create")
    public ProjectDTO create() throws AbstractException {
        return service.create();
    }

    @NotNull
    @Override
    @PutMapping("/add")
    public ProjectDTO add(@RequestBody @NotNull final ProjectDTO project) throws AbstractException {
        return service.add(project);
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void deleteById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        service.deleteById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        return service.existsById(id);
    }

    @NotNull
    @Override
    @GetMapping("/findById/{id}")
    public ProjectDTO findById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        return service.findById(id);
    }

    @NotNull
    @Override
    @PostMapping("/update/{id}")
    public ProjectDTO update(@RequestBody @NotNull final ProjectDTO project) throws AbstractException {
        return service.update(project);
    }

    @NotNull
    @Override
    @GetMapping("/findAll")
    public List<ProjectDTO> findAll() {
        return service.findAll();
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        service.clear();
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return service.count();
    }

}

