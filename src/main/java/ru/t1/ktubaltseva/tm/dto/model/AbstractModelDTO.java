package ru.t1.ktubaltseva.tm.dto.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.ktubaltseva.tm.enumerated.Status;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@MappedSuperclass
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {
        "id",
        "name",
        "description",
        "status",
        "created",
        "dateStart",
        "dateFinish"
})
public class AbstractModelDTO implements Serializable {

    @NotNull
    @XmlElement
    @Column(name = "name")
    protected String name = "";

    @Nullable
    @XmlElement
    @Column(name = "description")
    protected String description;

    @NotNull
    @XmlElement
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    protected Status status = Status.NOT_STARTED;

    @Nullable
    @XmlElement
    @Column(name = "dateStart")
    @XmlSchemaType(name = "dateTime")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateStart;

    @Nullable
    @XmlElement
    @Column(name = "dateFinish")
    @XmlSchemaType(name = "dateTime")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateFinish;

    @Id
    @NotNull
    @XmlElement
    @Column(name = "id", columnDefinition = "varchar(36)", nullable = false)
    private String id = UUID.randomUUID().toString();

    @NotNull
    @XmlElement
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    @Column(name = "created", columnDefinition = "timestamp default current_date", nullable = false)
    private Date created = new Date();

    public AbstractModelDTO(@Nullable final String name) {
        this.name = name;
    }

//    public AbstractModelDTO() {
//        this.name = id;
//    }

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        result += name;
        if (description != null)
            result += "\t(" + description + ")";
        result += "\t" + Status.toName(status) + "";
        return result;
    }

}
