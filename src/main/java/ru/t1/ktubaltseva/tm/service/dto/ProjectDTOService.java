package ru.t1.ktubaltseva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectDTOService;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.repository.dto.ProjectDTORepository;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDTOService extends AbstractDTOService<ProjectDTO, ProjectDTORepository> implements IProjectDTOService {

    @NotNull
    @Autowired
    private ProjectDTORepository repository;

    @Override
    public @NotNull ProjectDTO create() throws EntityNotFoundException {
        return add(new ProjectDTO());
    }
}
