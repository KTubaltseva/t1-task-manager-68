package ru.t1.ktubaltseva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.api.service.dto.ITaskDTOService;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.repository.dto.TaskDTORepository;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class TaskDTOService extends AbstractDTOService<TaskDTO, TaskDTORepository> implements ITaskDTOService {

    @NotNull
    @Autowired
    private TaskDTORepository repository;

    @Override
    public @NotNull TaskDTO create() throws EntityNotFoundException {
        return add(new TaskDTO());
    }

}
