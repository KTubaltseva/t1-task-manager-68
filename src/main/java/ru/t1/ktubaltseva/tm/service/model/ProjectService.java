package ru.t1.ktubaltseva.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.api.service.model.IProjectService;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.repository.model.ProjectRepository;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ProjectService extends AbstractService<Project, ProjectRepository> implements IProjectService {

    @NotNull
    @Autowired
    private ProjectRepository repository;

    @Override
    public @NotNull Project create() throws EntityNotFoundException {
        return create(new Project());
    }
}
