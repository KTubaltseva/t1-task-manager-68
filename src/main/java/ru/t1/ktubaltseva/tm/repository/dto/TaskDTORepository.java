package ru.t1.ktubaltseva.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;

@Repository
@Scope("prototype")
public interface TaskDTORepository extends AbstractDTORepository<TaskDTO> {

}
