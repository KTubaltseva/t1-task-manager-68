package ru.t1.ktubaltseva.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;

@Repository
@Scope("prototype")
public interface ProjectDTORepository extends AbstractDTORepository<ProjectDTO> {

}
