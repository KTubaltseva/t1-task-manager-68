package ru.t1.ktubaltseva.tm.controller.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectDTOService;
import ru.t1.ktubaltseva.tm.api.service.dto.ITaskDTOService;

@Controller
public class TasksController {

    @Autowired
    private ITaskDTOService taskService;

    @Autowired
    private IProjectDTOService projectService;

    @GetMapping("/tasks")
    public ModelAndView index() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-list");
        modelAndView.addObject("tasks", taskService.findAll());
        modelAndView.addObject("projectService", projectService);
        return modelAndView;
    }

}

