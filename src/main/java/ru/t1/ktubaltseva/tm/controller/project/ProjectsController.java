package ru.t1.ktubaltseva.tm.controller.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectDTOService;

@Controller
public class ProjectsController {

    @Autowired
    private IProjectDTOService projectService;

    @GetMapping("/projects")
    public ModelAndView index() {
        return new ModelAndView("project/project-list", "projects", projectService.findAll());
    }

}

