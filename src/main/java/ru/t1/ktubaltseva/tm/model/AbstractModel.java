package ru.t1.ktubaltseva.tm.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.ktubaltseva.tm.enumerated.Status;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@MappedSuperclass
public class AbstractModel implements Serializable {

    @Id
    @NotNull
    @Column(name = "id", columnDefinition = "varchar(36)", nullable = false)
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(name = "name")
    protected String name = "";

    @Nullable
    @Column(name = "description")
    protected String description;

    @NotNull
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    protected Status status = Status.NOT_STARTED;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    @Column(name = "created", columnDefinition = "timestamp default current_date", nullable = false)
    private Date created = new Date();

    @Nullable
    @Column(name = "dateStart")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateStart;

    @Nullable
    @Column(name = "dateFinish")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateFinish;

    public AbstractModel(@Nullable final String name) {
        this.name = name;
    }

    public AbstractModel() {
        this.name = id;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        result += name;
        if (description != null)
            result += "\t(" + description + ")";
        result += "\t" + Status.toName(status) + "";
        return result;
    }

}
